require 'eventmachine'

class FayeClient

  def self.publish(destination, body)
    EM.epoll
    EM.run {
      puts "Publish to #{destination}"

      faye_client = Faye::Client.new($faye_url)
      publication = faye_client.publish("/#{destination}", body)

      if Rails.env.test?
        publication.timeout 0, :timeout
      end

      publication.callback do
        Rails.logger.info '[PUBLISH SUCCEEDED]'
      end

      publication.errback do |error|
        Rails.logger.error "[PUBLISH FAILED] #{error.inspect}"
      end
    }
  rescue Exception => e
    Rails.logger.error e.message
    Rails.logger.error e.backtrace.join('\n')
  end

end