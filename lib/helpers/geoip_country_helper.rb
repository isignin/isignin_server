class GeoipCountryHelper
  def self.country_code(ip)
    return self.country_hash(ip)[:country_code]
  end

  def self.country_code2(ip)
    return self.country_hash(ip)[:country_code2]
  end

  def self.country_code3(ip)
    return self.country_hash(ip)[:country_code3]
  end

  def self.country_name(ip)
    return self.country_name(ip)[:country_name]
  end

  def self.continent_code(ip)
    return self.country_name(ip)[:continent_code]
  end

  def self.country_hash(ip)
    return GeoIP.new("#{Rails.root}/data/GeoIP.dat").country(ip).to_h
  end
end