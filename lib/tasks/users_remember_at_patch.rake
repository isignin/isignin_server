namespace :db do
  desc 'Fill db Users table remember_create_at column with default value'
  task fill_remember_created_at: :environment do
    fill_user_remember_created_at
  end
end

def fill_user_remember_created_at
  User.all.each do |user|
    if user.remember_created_at.blank?
      user.update(remember_created_at: Time.now)
    end
  end
end