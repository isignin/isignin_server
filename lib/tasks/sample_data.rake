namespace :db do
  desc 'Fill database with sample data'
  task populate_users: :environment do
    make_users
  end

  task populate_events: :environment do
    make_events
  end
end

def make_users
  100.times do |n|
    name = Faker::Name.name
    email = "example-#{n+1}@isignin.org"
    password = 'password'
    User.create!(username: name,
                 email: email,
                 password: password,
                 password_confirmation: password)
  end

  User.update_all(:confirmed_at => Time.now)
  User.find(2).update(locked_at: Time.now)
end

def make_events
  100.times do |n|
    name = Faker::Name.name
    address = Faker::Address.street_address
    start_at = Faker::Time.between(Time.now, 20.days.from_now, :all)
    lat = Faker::Address.latitude
    lon = Faker::Address.longitude
    temp_id = Random.rand(1..100)
    if User.find(temp_id)
      owner_id = temp_id
    else
      owner_id = 1
    end

    Event.create!(name: name,
                  address: address,
                  start_at: start_at,
                  lat: lat,
                  lon: lon,
                  owner_id: owner_id)
  end
end
