# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  username               :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  locked_at              :datetime
#  company                :string
#  country                :string
#  state                  :string
#  city                   :string
#  address1               :string
#  address2               :string
#  telephone              :string
#  phone                  :string
#  created_at             :datetime
#  updated_at             :datetime
#  dial_code              :string
#  api_token              :string
#  session_token          :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#

require 'rails_helper'

RSpec.describe User, :type => :model do
  before { @user = FactoryGirl.build(:user) }
  subject { @user }

  it { should respond_to(:email) }
  it { should respond_to(:username) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:reset_password_token) }
  it { should respond_to(:reset_password_sent_at) }
  it { should respond_to(:remember_created_at) }
  it { should respond_to(:sign_in_count) }
  it { should respond_to(:current_sign_in_at) }
  it { should respond_to(:last_sign_in_ip) }
  it { should respond_to(:confirmation_token) }
  it { should respond_to(:confirmed_at) }
  it { should respond_to(:confirmation_sent_at) }
  it { should respond_to(:unconfirmed_email) }
  it { should respond_to(:locked_at) }
  it { should respond_to(:title) }
  it { should respond_to(:country) }
  it { should respond_to(:state) }
  it { should respond_to(:city) }
  it { should respond_to(:address1) }
  it { should respond_to(:address2) }
  it { should respond_to(:telephone) }
  it { should respond_to(:phone) }
  it { should respond_to(:created_at) }
  it { should respond_to(:updated_at) }
  it { should respond_to(:dial_code) }
  it { should respond_to(:api_token) }
  it { should respond_to(:session_token) }
  it { should respond_to(:avatar_file_name) }
  it { should respond_to(:avatar_content_type) }
  it { should respond_to(:avatar_file_size) }
  it { should respond_to(:avatar_updated_at) }

  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_presence_of(:password_confirmation) }

  it { should be_valid }

  describe 'When username is not present' do
    before { subject.username = ' ' }
    it { expect(subject).not_to be_valid }
  end

  describe 'When email is not present' do
    before { subject.email = ' ' }
    it { expect(subject).not_to be_valid }
  end

  describe 'When password is not present' do
    before { subject.password = subject.password_confirmation = ' ' }
    it { expect(subject).not_to be_valid }
  end

  describe 'When username is too long' do
    before { subject.username = 'a' * 31 }
    it { expect(subject).not_to be_valid }
  end

  describe 'When email format is invalid' do
    it 'should be invalid' do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo. foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        subject.email = invalid_address
        expect(subject).not_to be_valid
      end
    end
  end

  describe 'When email format is valid' do
    it 'should be valid' do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        subject.email = valid_address
        expect(subject).to be_valid
      end
    end
  end

  describe 'When email address is already taken' do
    before do
      user_with_same_email = subject.dup
      user_with_same_email.email = subject.email.upcase
      user_with_same_email.save
    end
    it { expect(subject).not_to be_valid }
  end

  describe 'When password does not match confirmation' do
    before { subject.password_confirmation = 'mismatch' }
    it { expect(subject).not_to be_valid }
  end

  describe 'When password confirmation is nil' do
    before { subject.password_confirmation = nil }
    it { expect(subject).not_to be_valid }
  end

  describe 'When password is too short' do
    before { subject.password = subject.password_confirmation = 'a' * 5 }
    it { expect(subject).not_to be_valid }
  end

  describe 'confirmed_at' do
    before { subject.save }
    describe 'without confrimation' do
      it { expect(subject.confirmed_at).to eq(nil) }
    end

    describe 'with confirmation now' do
      before do
        subject.confirmed_at = Time.now
        subject.save
      end

      it 'should be activated for authentication' do
        expect(subject.active_for_authentication?).to eq(true)
      end
    end
  end
end
