# == Schema Information
#
# Table name: events
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  address    :string
#  start_at   :datetime         not null
#  end_at     :datetime
#  lat        :float            not null
#  lon        :float            not null
#  owner_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :event do
    sequence(:name) { |n| "event_#{n}" }
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
  end

end
