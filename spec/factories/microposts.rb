# == Schema Information
#
# Table name: microposts
#
#  id         :integer          not null, primary key
#  content    :string           not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :micropost do
    content 'Lorem ipsum'
  end

end
