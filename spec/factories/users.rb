# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  username               :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  locked_at              :datetime
#  company                :string
#  country                :string
#  state                  :string
#  city                   :string
#  address1               :string
#  address2               :string
#  telephone              :string
#  phone                  :string
#  created_at             :datetime
#  updated_at             :datetime
#  dial_code              :string
#  api_token              :string
#  session_token          :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#

FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "Person_#{n}@example.com" }
    sequence(:username) { |n| "Person #{n}" }
    password 'foobar'
    password_confirmation 'foobar'
  end
end
