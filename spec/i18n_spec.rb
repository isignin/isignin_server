require 'spec_helper'
require 'i18n/tasks'

describe 'I18n' do
  let(:i18n) { I18n::Tasks::BaseTask.new }
  let(:missing_keys_name) { i18n.missing_keys.key_names }

  it 'does not have missing keys' do
    expect(missing_keys_name).to be_empty,
      "Missing #{missing_keys_name.count} i18n keys, run `i18n-tasks missing' to show them"
  end
end
