module AuthHelper
  def http_basic_auth
    authorization = 'Basic ' + Base64::encode64("#{ENV["API_AUTH_NAME"]}:#{ENV["API_AUTH_PASSWORD"]}")

    @request.set_header 'kISAppTypeHeaderField', 'iSignin-debug'
    @request.set_header 'kISDeviceModelHeaderField', 'iPhone Simulator iPhone Simulator x86_64'
    @request.set_header 'kISAppVersionHeaderField', '0.0.1 (40)'
    @request.set_header 'kISSystemVersionHeaderField', 'iPhone OS 8.2'
    @request.env['kISCountryHeaderField'] = 'zh-Hans_US'
    @request.env['Accept'] = 'application/vnd.isignin.v1'
    @request.env['Content-Type'] = 'application/json'
    @request.env['HTTP_ACCEPT_LANGUAGE'] = 'en'
    @request.env['HTTP_AUTHORIZATION'] = authorization
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  def http_token_auth(user = nil)
    authorization = 'Token ' + User.find_by_id(user.id).api_token

    @request.set_header 'kISAppTypeHeaderField', 'iSignin-debug'
    @request.set_header 'kISDeviceModelHeaderField', 'iPhone Simulator iPhone Simulator x86_64'
    @request.set_header 'kISAppVersionHeaderField', '0.0.1 (40)'
    @request.set_header 'kISSystemVersionHeaderField', 'iPhone OS 8.2'
    @request.env['kISCountryHeaderField'] = 'zh-Hans_US'
    @request.env['Accept'] = 'application/vnd.isignin.v1'
    @request.env['Content-Type'] = 'application/json'
    @request.env['HTTP_ACCEPT_LANGUAGE'] = 'en'
    @request.env['HTTP_AUTHORIZATION'] = authorization
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  def sign_in_user(user = nil)
    http_basic_auth
    sign_in @user
  end
end

class ActionController::TestRequest
  def set_header(name, value)
    @env[name] = value
  end
end