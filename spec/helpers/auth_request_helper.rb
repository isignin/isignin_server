module AuthRequestHelper
  def http_basic_auth
    @env ||= {}
    authorization =  'Basic ' + Base64::encode64("#{ENV["API_AUTH_NAME"]}:#{ENV["API_AUTH_PASSWORD"]}")

    @env = {
        :kISAppTypeHeaderField => 'iSignin-debug',
        :kISDeviceModelHeaderField => 'iPhone Simulator iPhone Simulator x86_64',
        :kISAppVersionHeaderField => '0.0.1 (40)',
        :kISSystemVersionHeaderField => 'iPhone OS 8.2',
        :kISCountryHeaderField => 'zh-Hans_US',
        :Accept => 'application/vnd.isignin.v1',
        'Content-Type' => 'application/json',
        :HTTP_ACCEPT_LANGUAGE => 'en',
        :Authorization => authorization,
        :charset => 'utf-8'
    }
    @env['devise.mapping'] = Devise.mappings[:user]
    @env
  end

  def http_token_auth(user = nil)
    @env ||= {}
    authorization =  'Token ' + User.find_by_id(user.id).api_token
    @env = {
        :kISAppTypeHeaderField => 'iSignin-debug',
        :kISDeviceModelHeaderField => 'iPhone Simulator iPhone Simulator x86_64',
        :kISAppVersionHeaderField => '0.0.1 (40)',
        :kISSystemVersionHeaderField => 'iPhone OS 8.2',
        :kISCountryHeaderField => 'zh-Hans_US',
        :Accept => 'application/vnd.isignin.v1',
        'Content-Type' => 'application/json',
        :HTTP_ACCEPT_LANGUAGE => 'en',
        :Authorization => authorization,
        :charset => 'utf-8'
    }
    @env['devise.mapping'] = Devise.mappings[:user]
    @env
  end

  def sign_in_user(user = nil)
    post '/api/users/sign_in', { user: {
                                 email: user.email,
                                 password: 'foobar'} }.to_json, http_basic_auth
  end
end