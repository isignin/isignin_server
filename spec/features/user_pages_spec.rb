require 'rails_helper'

feature 'UserPages', :type => :feature do
  subject { page }
  describe 'UserPages' do
    describe 'home' do
      before { visit root_path }

      it { expect(subject).to have_button('Signup_btn') }
      it { expect(subject).to have_button('Signin_btn') }
      it { expect(subject).to have_field('user_email') }
      it { expect(subject).to have_field('user_password') }

      describe 'sign up' do
        before { click_button 'Signup_btn' }

        it { expect(subject).to have_selector('h4', text: t('users.registrations.signup_modal.Sign_up')) }

        describe 'with invalid information' do
          let(:submit) { t('users.registrations.signup_modal.Sign_up') }

          it 'should not create a user' do
            expect { click_button submit }.not_to change(User, :count)
          end
        end

        describe 'with valid information' do
          before do
            fill_in 'signup_email',            with: 'Example@user.com'
            fill_in 'signup_username',         with: 'Example User'
            fill_in 'signup_password',         with: '111111'
            fill_in 'signup_password_confirm', with: '111111'
          end

          it 'should create a user' do
            expect { click_button 'signup_submit' }.to change(User, :count).by(1)
          end

          describe 'after create a user' do
            before { click_button 'signup_submit' }
            it { expect(subject).to have_selector('li', text: t('devise.registrations.signed_up_but_unconfirmed'))}
            it { expect(subject).to have_selector('h2', text: t('users.confirmations.new.Resend_confirmation_instructions')) }
            it { expect(subject).to have_link(t('users.shared.links.forgot_your_password'))}
          end
        end
      end

      describe 'sign in' do
        describe 'unconfirmed user' do
          let(:unconfirmed_user) { FactoryGirl.create(:user) }
          before do
            fill_in 'user_email',    with: unconfirmed_user.email
            fill_in 'user_password', with: unconfirmed_user.password
            click_button 'Signin_btn'
          end

          it { expect(subject).to have_selector('li', text: t('devise.failure.unconfirmed')) }
          it { expect(subject).to have_link(t('users.shared.links.forgot_your_password'))}
          it { expect(subject).to have_link(t('users.shared.links.Didnt_receive_confirmation_instructions')) }
        end

        describe 'confirmed user' do
          let(:user) { FactoryGirl.create(:user) }
          before do
            user.confirm!
            fill_in 'user_email',    with: user.email
            fill_in 'user_password', with: user.password
            click_button 'Signin_btn'
          end

          it { expect(subject).to have_selector('li', text: t('devise.sessions.signed_in'))}
          it { expect(subject).to have_selector('h2', text: t('users.users.show.User')) }
          it { expect(subject).to have_link(t('layouts.header.Users')) }
          it { expect(subject).to have_button(t('layouts.header.Account')) }

          describe 'click account btn' do
            before do
              click_button 'account-btn'
            end

            it { expect(subject).to have_link(t('layouts.header.Profile')) }
            it { expect(subject).to have_link(t('layouts.header.Settings')) }
            it { expect(subject).to have_link(t('layouts.header.Sign_out')) }

            describe 'click profile' do
              before { click_link t('layouts.header.Profile') }

              it { expect(subject).to have_content(user.email) }
              it { expect(subject).to have_content(user.username) }
            end

            describe 'click settings' do
              before { click_link t('layouts.header.Settings') }
              it { expect(subject).to have_content(t('users.registrations.edit.Edit')) }

              describe 'update username successfully' do
                before do
                  fill_in 'user_username', with: "#{user.username}_change"
                  fill_in 'user_current_password', with: 'foobar'
                  click_button 'update_button'
                end

                it { expect(subject).to have_content(t('devise.registrations.updated')) }
              end

              describe 'update password successfully' do
                before do
                  fill_in 'user_username', with: user.username
                  fill_in 'user_password', with: 'foobar2'
                  fill_in 'user_password_confirmation', with: 'foobar2'
                  fill_in 'user_current_password', with: 'foobar'
                  click_button 'update_button'
                end

                it { expect(subject).to have_content(t('devise.registrations.updated')) }
              end

              describe 'update failed because of fault password' do
                before do
                  fill_in 'user_username', with: "#{user.username}_change"
                  fill_in 'user_current_password', with: 'wrong_password'
                  click_button 'update_button'
                end

                it { expect(subject).not_to have_content(t('devise.registrations.updated')) }
              end

              describe 'update user avatar' do
                #TODO
              end
            end

            describe 'click sign out' do
              before do
                click_link t('layouts.header.Sign_out')
              end

              it { expect(subject).to have_selector('li', t('devise.sessions.signed_out')) }
              it { expect(current_path).to eq(root_path) }
            end
          end
        end
      end

      describe 'help page' do
        let(:user) { FactoryGirl.create(:user) }
        before do
          visit help_path
        end

        describe 'forget password' do
          before { click_link t('static_pages.help.forgot_your_password') }

          describe 'not confirmed email address' do
            before do
              fill_in 'forgot_password_email', with: user.email
              click_button 'forgot_password_button'
            end

            it { expect(subject).to have_selector('li', t('devise.passwords.send_instructions')) }
          end

          describe 'non-existing user' do
            before do
              fill_in 'forgot_password_email', with: 'non-existing@user.com'
              click_button 'forgot_password_button'
            end

            it { expect(subject).to have_selector('li', t('errors.messages.not_found')) }
          end

          describe 'existing user' do
            before do
              user.confirm!
              fill_in 'forgot_password_email', with: user.email
              click_button 'forgot_password_button'
            end

            it { expect(subject).to have_selector('li', t('devise.passwords.send_instructions')) }
          end
        end

        describe "Didn't receive confirmation instructions page" do
          before { click_link t('static_pages.help.Didnt_receive_confirmation_instructions') }

          describe 'already confirmed email address' do
            before do
              user.confirm!
              fill_in 'resend_confirmation_email', with: user.email
              click_button 'resend_confirmation_button'
            end

            it { expect(subject).to have_selector('li', t('errors.messages.already_confirmed')) }
          end

          describe 'non-existing email address' do
            before do
              fill_in 'resend_confirmation_email', with: 'non-existing@user.com'
              click_button 'resend_confirmation_button'
            end

            it { expect(subject).to have_selector('li', t('errors.messages.not_found'))}
          end

          describe 'existing not-confirmed email address' do
            before do
              fill_in 'resend_confirmation_email', with: user.email
              click_button 'resend_confirmation_button'
            end

            it { expect(subject).to have_selector('li', t('devise.confirmations.send_instructions')) }
          end
        end
      end
    end
  end
end