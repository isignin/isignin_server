require 'rails_helper'

feature 'StaticPages', :type => :feature do
  describe 'Static pages' do
    include ApplicationHelper
    subject { page }

    describe 'Home page' do
      before do
        visit home_path
      end

      it { expect(subject).to have_title(full_title(''))}
    end

    describe 'Help page' do
      before do
        visit help_path
      end

      it { expect(subject).to have_title(full_title(t('static_pages.help.title'))) }
    end

    describe 'About page' do
      before do
        visit about_path
      end

      it { expect(subject).to have_title(full_title(t('static_pages.about.title'))) }
    end

    describe 'Contact page' do
      before do
        visit contact_path
      end

      it { expect(subject).to have_title(full_title(t('static_pages.contact.title'))) }
    end

    describe 'Terms page' do
      before do
        visit terms_path
      end

      it { expect(subject).to have_title(full_title(t('static_pages.terms.title'))) }
    end
  end
end
