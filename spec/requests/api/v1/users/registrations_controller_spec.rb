require 'rails_helper'

describe 'registrations controller', :type => :request do
  describe 'sign up' do

    it 'should get 201 Created' do
      user = {
          email: 'example@user.com',
          username: 'example user',
          password: '111111',
          password_confirmation: '111111'
      }
      post '/api/users/sign_up', { user: user }.to_json, http_basic_auth
      expect(response.status).to eq(201)
    end

    it 'should get error 422' do
      temp = FactoryGirl.create(:user)
      user = {
          email: temp.email,
          username: temp.username,
          password: 'foobar',
          password_confirmation: 'foobar'
      }
      post '/api/users/sign_up', { user: user }.to_json, http_basic_auth
      expect(response.status).to eq(422)
    end
  end

  describe 'update' do
    let(:user) { FactoryGirl.create(:user)}
    before do
      user.confirm!
      sign_in_user user
    end

    describe 'without current password to confirm' do
      it 'should get error 422' do
        user_params = { username: 'changed username',
                        title: 'company',
                        country: 'China',
                        state: 'Fujian',
                        city: 'Xiamen',
                        address1: 'xyz street',
                        address2: 'abc block',
                        dial_code: '001',
                        phone: '1234567890',
                        telephone: '1234567890' }
        patch '/api/users', { user: user_params }.to_json, http_token_auth(user)
        expect(response.status).to eq(422)
      end
    end

    describe 'with current password to confirm' do
      it 'should get 200' do
        user_params = { username: 'changed username',
                        title: 'company',
                        country: 'China',
                        state: 'Fujian',
                        city: 'Xiamen',
                        address1: 'xyz street',
                        address2: 'abc block',
                        dial_code: '001',
                        phone: '1234567890',
                        telephone: '1234567890',
                        current_password: 'foobar' }
        patch '/api/users', { user: user_params }.to_json, http_token_auth(user)
        expect(response.status).to eq(200)
        expect(response.body).to eq({ user:
                                          { id: user.id,
                                            email: user.email,
                                            username: user_params[:username],
                                            title: user_params[:title],
                                            country: user_params[:country],
                                            state: user_params[:state],
                                            city: user_params[:city],
                                            address1: user_params[:address1],
                                            address2: user_params[:address2],
                                            dial_code: user_params[:dial_code],
                                            phone: user_params[:phone],
                                            telephone: user_params[:telephone] } }.to_json)
      end
    end
  end
end