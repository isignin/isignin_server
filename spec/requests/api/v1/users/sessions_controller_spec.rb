require 'rails_helper'

describe 'sessions controller', type: :request do
  describe 'sign in' do
    before do
      @user = FactoryGirl.create(:user)
    end

    it 'should get 401 error if not confirm the email address' do
      user = {
          email: @user.email,
          password: 'foobar'
      }
      post '/api/users/sign_in', { user: user }.to_json, http_basic_auth
      expect(response.status).to eq(401)
    end

    it 'should get 201 Created' do
      @user.confirm!
      user = {
          email: @user.email,
          password: 'foobar'
      }
      post '/api/users/sign_in', { user: user }.to_json, http_basic_auth
      expect(response.status).to eq(201)
      expect(response.body).to eq({ user: {
                                            id:        @user.id,
                                            email:     @user.email,
                                            username:  @user.username,
                                            title:     @user.title,
                                            country:   @user.country,
                                            state:     @user.state,
                                            city:      @user.city,
                                            address1:  @user.address1,
                                            address2:  @user.address2,
                                            telephone: @user.telephone,
                                            phone:     @user.phone,
                                            dial_code: @user.dial_code,
                                            api_token: User.find_by_id(@user.id).api_token
                                        }
                                  }.to_json)
    end

    it 'should get 401 error if not right password' do
      @user.confirm!
      user = {
          email: @user.email,
          password: 'fault_password'
      }
      post '/api/users/sign_in', { user: user }.to_json, http_basic_auth
      expect(response.status).to eq(401)
    end
  end
end