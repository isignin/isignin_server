require 'rails_helper'

describe 'password controller', :type => :request do
  describe 'retrieve password' do
    let(:user) { FactoryGirl.create(:user) }

    it 'should get 201 created with confirmed user' do
      user.confirm!
      post '/api/users/password', { user: { email: user.email } }.to_json, http_basic_auth


      expect(response.status).to eq(201)
    end

    it 'should get 201 created with unconfirmed user' do
      post '/api/users/password', { user: { email: user.email } }.to_json, http_basic_auth
      expect(response.status).to eq(201)
    end

    it 'should get error 500 with non-existing user' do
      post '/api/users/password', { user: { email: 'non-existing@user.com' } }.to_json, http_basic_auth
      expect(response.status).to eq(500)
    end
  end
end