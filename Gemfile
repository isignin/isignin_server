source "https://rubygems.org"

ruby '2.2.0'

#A simple and lightweight mixin library for Sass.
gem 'bourbon', '4.1.0'

#CoffeeScript adapter for the Rails asset pipeline
gem 'coffee-rails', '4.1.0'

#Background processing
gem 'sidekiq', '3.3.4'

#ActiveRecord backend integration for DelayedJob 3.0+
gem 'email_validator', '1.5.0'

#translation and localization
gem 'i18n-tasks', '0.3.5'

#A gem to automate using jQuery with Rails
gem 'jquery-rails', '4.0.3'

#PostgreSQL
gem 'pg', '0.18.1'
gem 'rails', '4.2.0'

#Intercept recipients when delivering email with the Mail gem
gem 'recipient_interceptor' , '0.1.2'
gem 'sass-rails', '5.0'

# Ruby wrapper for UglifyJS JavaScript compressor
gem 'uglifier', '2.7.0'

# annotate model
gem 'annotate', '2.6.5'
gem 'passenger', '4.0.58'

# api pagination
gem 'kaminari', '0.16.2'

# api data in view
gem 'jbuilder', '2.2.13'
gem 'sprockets-rails', '2.2.4'

# create user password hash
gem 'bcrypt-ruby', '3.1.2'
gem 'faker', '1.4.3'
gem 'devise', '3.4.1'

#Abort requests that are taking too long
gem 'rack-timeout', '0.2.0'
gem 'normalize-rails', '3.0.1'

# REST API
gem 'geocoder', '1.2.9'
gem 'http_accept_language', '2.0.5'

# Sync
gem 'faye', '1.1.1'
gem 'redis', '3.2.1'
gem 'thin', '1.6.3'
gem 'foreman', '0.78.0'

# Image upload
gem 'paperclip', '4.3'

# Service
gem 'fog', '1.32.0'

group :development do
  # Spring is a Rails application preloader,
  # it speeds up development by keeping your application running in the background
  gem 'spring', '1.3.3'
  gem 'spring-commands-rspec', '1.0.4'
  gem 'web-console', '2.0.0'
end

group :development, :test do
  gem 'awesome_print', '1.6.1'
  #Patch-level verification for Bundler
  gem 'bundler-audit', '0.3.1', require: false
  gem 'byebug', '3.5.1'
  gem 'factory_girl_rails', '4.5.0'
  gem 'pry-rails', '0.3.3'
  gem 'rspec-rails', '3.1.0'
  gem 'figaro', '1.1.0'
end

group :test do
  gem 'capybara-webkit', '1.4.1'
  gem 'database_cleaner', '1.4.0'
  gem 'shoulda-matchers', '2.8.0', require: false
  gem 'timecop','0.7.1'
  gem 'webmock', '1.20.4'
  gem 'cucumber-rails', '1.2.1', :require => false
end

group :staging, :production do
  #Makes running your Rails app easier
  gem 'rails_12factor'
end
