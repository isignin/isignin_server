class ChangeTitleToCompanyInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :title, :company
  end
end
