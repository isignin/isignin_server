class AddDialCodeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :dial_code, :string
  end
end
