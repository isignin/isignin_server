class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name, null: false
      t.string :address
      t.datetime :start_at, null: false
      t.datetime :end_at
      t.float :lat, null: false
      t.float :lon, null: false
      t.integer :owner_id, null: false
      t.integer :group_id, null: false

      t.timestamps null: false
    end
  end
end
