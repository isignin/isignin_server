class AddParameterIndexesOnEvents < ActiveRecord::Migration
  def change
    add_index :events, :name
    add_index :events, :address
    add_index :events, :start_at
    add_index :events, :end_at
    add_index :events, :lat
    add_index :events, :lon
    add_index :events, :owner_id
  end
end
