class DropColumnGroupIdInEvents < ActiveRecord::Migration
  def change
    remove_column :events, :group_id
  end
end
