class CreateAttendanceRelationships < ActiveRecord::Migration
  def change
    create_table :attendance_relationships do |t|
      t.integer :event_id, null: false
      t.integer :attendee_id, null: false

      t.timestamps null: false
    end
    add_index :attendance_relationships, [:event_id, :attendee_id]
  end
end
