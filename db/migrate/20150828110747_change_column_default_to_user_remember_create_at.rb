class ChangeColumnDefaultToUserRememberCreateAt < ActiveRecord::Migration
  def change
    change_column_default :users, :remember_created_at, Time.now
  end
end
