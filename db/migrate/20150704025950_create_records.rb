class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.integer :user_id, null: false
      t.integer :event_id, null: false
      t.integer :action_type, null: false

      t.timestamps null: false
    end
    add_index :records, [:user_id, :event_id]
  end
end
