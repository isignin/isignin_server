# ISignin API

## 1. Both client app requestSerializer and responseSerializer should be JSON data 
## 2. iOS setup five environment:
   1. DB_ENVIRONMENT_DEBUG_LOCAL: debug version locally if you want to setup local server
   2. DB_ENVIRONMENT_DEBUG_SERVER: debug version with staging server
   3. DB_ENVIRONMENT_ALPHA: product version with staging server
   4. DB_ENVIRONMENT_BETA: product version with beta server
   5. DB_ENVIRONMENT_APPSTORE: product version the release server(not setup yet)
   6. Note: API_AUTH_NAME and API_AUTH_PASSWORD are for Basic Auth; HockeyappBetaID and HockeyAppliveID is for release platform hockeyapp, for android relase should refer to their website for more information.

   
```
#!c

   #if   DB_ENVIRONMENT == DB_ENVIRONMENT_DEBUG_LOCAL
   NSString * const DataCenterUrl = @"http://localhost:3000/api/";
   NSString * const API_AUTH_NAME = @"damonyuan";
   NSString * const API_AUTH_PASSWORD = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
   NSString * const HockeyappBetaID    = @"0dba96e85595d82223b18f2e81f7e25d";
   NSString * const HockeyappAppLiveID = @"096420e7a88e4edd4281c1a071954231";
   #elif DB_ENVIRONMENT == DB_ENVIRONMENT_DEBUG_SERVER
   NSString * const DataCenterUrl = @"https://isigninstaging.herokuapp.com/api/";
   NSString * const API_AUTH_NAME = @"damonyuan";
   NSString * const API_AUTH_PASSWORD = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
   NSString * const HockeyappBetaID    = @"0dba96e85595d82223b18f2e81f7e25d";
   NSString * const HockeyappAppLiveID = @"096420e7a88e4edd4281c1a071954231";
   #elif DB_ENVIRONMENT == DB_ENVIRONMENT_ALPHA
   NSString * const DataCenterUrl = @"https://isigninstaging.herokuapp.com/api/";
   NSString * const API_AUTH_NAME = @"damonyuan";
   NSString * const API_AUTH_PASSWORD  = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
   NSString * const HockeyappBetaID    = @"0dba96e85595d82223b18f2e81f7e25d";
   NSString * const HockeyappAppLiveID = @"096420e7a88e4edd4281c1a071954231";
   #elif DB_ENVIRONMENT == DB_ENVIRONMENT_BETA
   NSString * const DataCenterUrl = @"https://isigninbeta.herokuapp.com/api/";
   NSString * const API_AUTH_NAME = @"damonyuan";
   NSString * const API_AUTH_PASSWORD  = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
   NSString * const HockeyappBetaID    = @"546ca9222350f171f19ec6fa64023465";
   NSString * const HockeyappAppLiveID = @"096420e7a88e4edd4281c1a071954231";
   #elif DB_ENVIRONMENT == DB_ENVIRONMENT_APPSTORE
   NSString * const DataCenterUrl = @"";
   NSString * const API_AUTH_NAME = @"";
   NSString * const API_AUTH_PASSWORD  = @"";
   NSString * const HockeyappBetaID    = @"546ca9222350f171f19ec6fa64023465";
   NSString * const HockeyappAppLiveID = @"096420e7a88e4edd4281c1a071954231";
```


## 3. request Headers:
   1. {"kISAppTypeHeaderField" : <appTypeName> } 
      
      eg: "iSignin-debug" for DB_ENVIRONMENT_DEBUG_LOCAL and DB_ENVIRONMENT_DEBUG_SERVER; "iSignin-alpha" for DB_ENVIRONMENT_ALPHA; "iSignin-beta" for DB_ENVIRONMENT_BETA; "iSignin" for DB_ENVIRONMENT_APPSTORE

   2. {"kISDeviceModelHeaderField" : <iOS/Android device model>}
   3. {"kISAppVersionHeaderField" : <iOS/Android app version>}
   4. {"kISSystemVersionHeaderField" : <iOS/Android system version>}
   5. {"kISCountryHeaderField" : <iOS/Android country>}
   6. {"Accept" : "application/vnd.isignin.v1"}

      Note: this is used by server to decide the api version

   7. {"Content-Type" : "application/json"}
   8. {"HTTP_ACCEPT_LANGUAGE" : <iOS/Android country>}

      Note: this is important and is used by server side to decide the language and return the response with the right language.

   9. {"Authorization" :  <Basic or Token>}
      
        Note: for security reason we encode the string into utf8 data
        
        string = API_AUTH_NAME:API_AUTH_PASSWORD;
        data = string.utf8StringEncoding;
        encodedString = data.base64EncodedString
        If user has not login, should use basic auth: {"Authorization" : "Basic encodedString"};
        If user has login, should use token auth: {"Authorization" : "Token <token returned by server and stored in a secured place>"}
   
#API

##1. sign up
###Purpose
==========
  
  Create new user account

###Test Example
==========


```
#!sign up

user = {
          email: 'example@user.com',
          username: 'example user',
          password: '111111',
          password_confirmation: '111111'
      }

post '/api/users/sign_up', { user: user }.to_json, http_basic_auth
expect(response.status).to eq(201)

```

##2. sign in 
###Purpose
==========

  Sign in a user account

###Test Example
==========


```
#!sign in

@user #already confirmed user

user = {
          email: @user.email,
          password: 'foobar'
      }

post '/api/users/sign_in', { user: user }.to_json, http_basic_auth
      expect(response.status).to eq(201)
      expect(response.body).to eq({ user: {
                                            id:        @user.id,
                                            email:     @user.email,
                                            username:  @user.username,
                                            title:     @user.title,
                                            country:   @user.country,
                                            state:     @user.state,
                                            city:      @user.city,
                                            address1:  @user.address1,
                                            address2:  @user.address2,
                                            telephone: @user.telephone,
                                            phone:     @user.phone,
                                            dial_code: @user.dial_code,
                                            api_token: User.find_by_id(@user.id).api_token
                                        }
                                  }.to_json)

```

##3. retrieve password
###Purpose
==========

  reset the user's password through email 

###Test example
==========


```
#!retrieve password

@user #already confirmed user

post '/api/users/password', { user: { email: user.email } }.to_json, http_basic_auth
expect(response.status).to eq(201)

```

##4. update user profile
###Purpose
==========

  update user's attribute in profile

###Test example
==========

```
#!update user attribute

@user #already confirmed user
sign_in_user(user) # then should use token auth

user = {
          email: @user.email,
          password: 'foobar'
      }

user_params = { username: 'changed username',
                        title: 'company',
                        country: 'China',
                        state: 'Fujian',
                        city: 'Xiamen',
                        address1: 'xyz street',
                        address2: 'abc block',
                        dial_code: '001',
                        phone: '1234567890',
                        telephone: '1234567890',
                        current_password: 'foobar' }

patch '/api/users', { user: user_params }.to_json, http_token_auth(user)
expect(response.status).to eq(200)
expect(response.body).to eq({ user: { id: user.id,
                                      email: user.email,
                                      username: user_params[:username],
                                      title: user_params[:title],
                                      country: user_params[:country],
                                      state: user_params[:state],
                                      city: user_params[:city],
                                      address1: user_params[:address1],
                                      address2: user_params[:address2],
                                      dial_code: user_params[:dial_code],
                                      phone: user_params[:phone],
                                      telephone: user_params[:telephone] } }.to_json)
                                          
```