# == Route Map
#
#                   Prefix Verb   URI Pattern                       Controller#Action
#                     home GET    /home(.:format)                   web/static_pages#home
#                     help GET    /help(.:format)                   web/static_pages#help
#                  contact GET    /contact(.:format)                web/static_pages#contact
#                    about GET    /about(.:format)                  web/static_pages#about
#                    terms GET    /terms(.:format)                  web/static_pages#terms
#         new_user_session GET    /users/sign_in(.:format)          web/users/sessions#new
#             user_session POST   /users/sign_in(.:format)          web/users/sessions#create
#     destroy_user_session DELETE /users/sign_out(.:format)         web/users/sessions#destroy
#            user_password POST   /users/password(.:format)         web/users/passwords#create
#        new_user_password GET    /users/password/new(.:format)     web/users/passwords#new
#       edit_user_password GET    /users/password/edit(.:format)    web/users/passwords#edit
#                          PATCH  /users/password(.:format)         web/users/passwords#update
#                          PUT    /users/password(.:format)         web/users/passwords#update
# cancel_user_registration GET    /users/cancel(.:format)           web/users/registrations#cancel
#        user_registration POST   /users(.:format)                  web/users/registrations#create
#    new_user_registration GET    /users/sign_up(.:format)          web/users/registrations#new
#   edit_user_registration GET    /users/edit(.:format)             web/users/registrations#edit
#                          PATCH  /users(.:format)                  web/users/registrations#update
#                          PUT    /users(.:format)                  web/users/registrations#update
#                          DELETE /users(.:format)                  web/users/registrations#destroy
#        user_confirmation POST   /users/confirmation(.:format)     web/users/confirmations#create
#    new_user_confirmation GET    /users/confirmation/new(.:format) web/users/confirmations#new
#                          GET    /users/confirmation(.:format)     web/users/confirmations#show
#                    users GET    /users(.:format)                  web/users/users#index
#                     user GET    /users/:id(.:format)              web/users/users#show
#                   events GET    /events(.:format)                 web/events#index
#                          POST   /events(.:format)                 web/events#create
#                    event GET    /events/:id(.:format)             web/events#show
#                     root GET    /                                 web/static_pages#home
#        api_users_sign_up POST   /api/users/sign_up(.:format)      api/v1/users/registrations#create {:format=>"json"}
#                api_users PUT    /api/users(.:format)              api/v1/users/registrations#update {:format=>"json"}
#                          PATCH  /api/users(.:format)              api/v1/users/registrations#update {:format=>"json"}
#        api_users_sign_in POST   /api/users/sign_in(.:format)      api/v1/users/sessions#create {:format=>"json"}
#       api_users_password POST   /api/users/password(.:format)     api/v1/users/passwords#create {:format=>"json"}
#                          GET    /api/users(.:format)              api/v1/users/users#index {:format=>"json"}
#                 api_user GET    /api/users/:id(.:format)          api/v1/users/users#show {:id=>/\d+/, :format=>"json"}
#               api_events GET    /api/events(.:format)             api/v1/events#index {:format=>"json"}
#                          POST   /api/events(.:format)             api/v1/events#create {:format=>"json"}
#                api_event GET    /api/events/:id(.:format)         api/v1/events#show {:id=>/\d+/, :format=>"json"}
#                          PATCH  /api/events/:id(.:format)         api/v1/events#update {:id=>/\d+/, :format=>"json"}
#                          PUT    /api/events/:id(.:format)         api/v1/events#update {:id=>/\d+/, :format=>"json"}
#      api_events_nearests GET    /api/events/nearests(.:format)    api/v1/events/nearests#index {:format=>"json"}
#

require 'api_constraints'

ISignin::Application.routes.draw do
  #here is the website routes
  scope module: :web do
    match '/home',    to: 'static_pages#home',    via: :GET
    match '/help',    to: 'static_pages#help',    via: :GET
    match '/contact', to: 'static_pages#contact', via: :GET
    match '/about',   to: 'static_pages#about',   via: :GET
    match '/terms',   to: 'static_pages#terms',   via: :GET

    devise_for :users, controllers: { confirmations: 'web/users/confirmations',
                                      passwords: 'web/users/passwords',
                                      registrations: 'web/users/registrations',
                                      sessions: 'web/users/sessions' }

    scope module: :users do
      resources :users,   only: [:show, :index]
    end

    resources :events, only: [:show, :index, :create]

    # The priority is based upon order of creation:
    # first created -> highest priority.

    # Sample of regular route:
    #   match 'products/:id' => 'catalog#view'
    # Keep in mind you can assign values other than :controller and :action

    # Sample of named route:
    #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
    # This route can be invoked with purchase_url(:id => product.id)

    # Sample resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Sample resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection d
    # o
    #       get 'sold'
    #     end
    #   end

    # Sample resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Sample resource route with more complex sub-resources
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', :on => :collection
    #     end
    #   end

    # Sample resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end

    # See how all your routes lay out with "rake routes"

    # You can have the root of your site routed with "root"
    root 'static_pages#home'

    # This is a legacy wild controller route that's not recommended for RESTful applications.
    # Note: This route will make all actions in every controller accessible via GET requests.
    # match ':controller(/:action(/:id))(.:format)'
  end

  # Here is the API routes
  namespace :api, defaults: { format: 'json' } do
    # Here is version 1
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      devise_scope :user do
        post  'users/sign_up',  to: 'users/registrations#create'
        put   'users',          to: 'users/registrations#update'
        patch 'users',          to: 'users/registrations#update'
        post  'users/sign_in',  to: 'users/sessions#create'
        post  'users/password', to: 'users/passwords#create'
      end

      scope module: :users, constraints: {id: /\d+/} do
        resources :users, only: [:show, :index]
      end

      resources :events, only: [:show, :index, :create, :update], constraints: {id: /\d+/}
      match 'events/nearests', to: 'events/nearests#index', via: :GET

    end
  end
end
