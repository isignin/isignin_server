require_relative "production"

# Mail.register_interceptor(
#     RecipientInterceptor.new(ENV.fetch("EMAIL_RECIPIENTS"))
# )

Rails.application.configure do
  # ...

  config.action_mailer.default_url_options = { host: ENV.fetch("HOST") }
  config.action_mailer.smtp_settings = SMTP_SETTINGS_STAGING
  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug
end