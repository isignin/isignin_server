class Api::V1::EventsController < Api::ApiController
  # show
  # index
  # create
  # update

  private

  def query_params
    params.permit(:owner_id)
    if params[:owner_id].blank?
      {owner_id: current_user.id}
    else
      {owner_id: params[:owner_id]}
    end
  end

  def page_params
    params.permit(:page, :page_size, :owner_id)
    {page: params[:page], page_size: params[:page_size]}
  end

  def event_params
    params[:event].permit(:name,
                          :start_at,
                          :end_at,
                          :lat,
                          :lon,
                          :owner_id)
  end
end