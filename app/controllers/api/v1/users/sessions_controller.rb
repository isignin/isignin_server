class Api::V1::Users::SessionsController < Devise::SessionsController
  http_basic_authenticate_with name:ENV["API_AUTH_NAME"],
                               password:ENV["API_AUTH_PASSWORD"],
                               :only => [:create]
  before_filter :configure_sign_in_params, :sign_out_before_sign_in, only: [:create]
  respond_to :json

  def create
    self.resource = warden.authenticate!(:scope => :user)

    if self.resource
      sign_in(:user, resource)
      resource.reset_api_authentication_token!
      respond_with resource,
                   status: 201
    end
  end

  protected

  def configure_sign_in_params
    devise_parameter_sanitizer.for(:sign_in) << :email << :password << :remember_me
  end

  def sign_out_before_sign_in
    sign_out(:user) if current_user
  end

end