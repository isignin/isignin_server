class Api::V1::Users::RegistrationsController < Devise::RegistrationsController

  http_basic_authenticate_with name:ENV["API_AUTH_NAME"],
                               password:ENV["API_AUTH_PASSWORD"],
                               :only => [:create]
  before_action :authenticate_user_with_token!,
                :configure_account_update_params,
                only: [:update]
  respond_to :json

  # POST /resource
  def create
    build_resource(user_params)
    if resource.save
      render json: { success: true,
                     info: 'Sign up success',
                     data: [] },
             status: :created
    else
      render json: { success: false,
                     info: 'Sign up fail',
                     data: resource.errors.messages },
             status: :unprocessable_entity
    end
  end

  # PUT /resource
  def update
    self.resource = User.to_adapter.get!(send(:"current_user").to_key)
    resource_updated = update_resource(resource, account_update_params)
    if resource_updated
      sign_in resource_name, resource, bypass: true
      respond_with resource
    else
      clean_up_passwords resource
      render json: { success: false,
                     info: 'Update registration fail',
                     data: resource.errors.messages },
             status: :unprocessable_entity
    end
  end

  protected

  def user_params
    params.require(:user).permit(:email, :username, :password, :password_confirmation)
  end

  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update) << :username << :password << :password_confirmation << :current_password << :title << :country << :state << :city << :address1 << :address2 << :telephone << :phone << :dial_code
  end

  # You can put the params you want to permit in the empty array.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) << :email << :username << :password << :password_confirmation
  end

end