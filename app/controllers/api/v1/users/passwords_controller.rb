class Api::V1::Users::PasswordsController < Devise::PasswordsController

  http_basic_authenticate_with name:ENV["API_AUTH_NAME"],
                               password:ENV["API_AUTH_PASSWORD"],
                               :only => [:create]
  respond_to :json

  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    if successfully_sent?(resource)
      render json: { success: true,
                     info: 'A password reset letter is sending to your email address',
                     data: [] },
             status: 201
    else
      render json: {
                 success: false,
                 info: 'reset password failure',
                 data: resource.errors.messages
             },
             status: :internal_server_error
    end
  end
end