class Api::V1::Events::NearestsController < Api::ApiController
  skip_before_action :set_resource
  # index
  def index
    @events = Event.near([nearsts_params[:lat], nearsts_params[:lon]],
                         nearsts_params[:radius],
                         units: :km)
    respond_with @events
  end

  private

  def nearsts_params
    params[:nearest].permit(:lat, :lon, :radius)
  end
end
