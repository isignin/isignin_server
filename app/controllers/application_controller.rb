class ApplicationController < ActionController::Base
  before_filter :set_locale
  layout 'web/layouts/application'

  #Force signout to prevent CSRF attacks
  def handle_unverified_request
    sign_out
  end

  # The path used after sign in
  def after_sign_in_path_for(resource_or_scope)
    unless json_request?
      if user_signed_in?
        user_path(resource_or_scope)
      else
        root_path
      end
    end
  end

  def authenticate_user_with_token!
    authenticate_user!
    authenticate_token!
  end

  private

  # Token authentication
  def authenticate_token!
    if json_request?
      authenticate_or_request_with_http_token do |token, options|
        if current_user.api_token.eql? token
          true
        else
          sign_out :user
          render status: 401,
                 json: {
                   success: false,
                   info: 'Login Failed',
                   data: t('devise.failure.unauthenticated')
                 }
        end
      end
    else
      unless current_user.session_token.eql? cookies[:session_authentication_token]
        sign_out :user
        flash[:error] = t('devise.failure.unauthenticated')
        redirect_to root_path
      end
    end
  end

  # Locales
  def set_locale
    I18n.locale = http_accept_language.compatible_language_from(I18n.available_locales)
  end

  def json_request?
    request.format.json?
  end
end
