class Web::StaticPagesController < Web::WebController
  skip_before_action :authenticate_user_with_token!

  def home
  end

  def help
  end

  def about
  end

  def contact
  end

  def terms

  end

end
