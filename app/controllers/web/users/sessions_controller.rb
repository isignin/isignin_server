class Web::Users::SessionsController < Devise::SessionsController
  before_filter :authenticate_user_with_token!, only: [:destroy]
  before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    super do
      resource.reset_session_authentication_token!
      cookies.permanent[:session_authentication_token] = resource.session_token
    end
  end

  # DELETE /resource/sign_out
  def destroy
    super do
      cookies.delete(:session_authentication_token)
    end
  end

  protected

  # You can put the params you want to permit in the empty array.
  def configure_sign_in_params
    devise_parameter_sanitizer.for(:sign_in) << :email << :password << :remember_me
  end
end
