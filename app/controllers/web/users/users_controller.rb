class Web::Users::UsersController < Web::WebController

  def index
    @users = User.active_users.page(params[:page])
                              .per(params[:page_size])
                              .order('id ASC')
  end

  def show
    @user = User.find_by_id(params[:id])
    unless @user.locked_at.nil?
      flash[:error] = t 'errors.messages.not_found'
      redirect_to users_path
    end
  end
end
