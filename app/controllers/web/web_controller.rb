class Web::WebController < ApplicationController
  before_action :authenticate_user_with_token!
  protect_from_forgery with: :exception

end