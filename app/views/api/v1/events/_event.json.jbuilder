json.cache! event do
  json.id event.id
  json.name event.name
  json.start_at event.start_at
  json.end_at event.end_at
  json.lat event.lat
  json.lon event.lon
  json.owner_id event.owner_id
end