json.user do
  json.id        @user.id
  json.email     @user.email
  json.username  @user.username
  json.company   @user.company
  json.country   @user.country
  json.state     @user.state
  json.city      @user.city
  json.address1  @user.address1
  json.address2  @user.address2
  json.telephone @user.telephone
  json.phone     @user.phone
  json.dial_code @user.dial_code
  json.api_token @user.api_token
  json.avatar    avatar_url_for @user
end