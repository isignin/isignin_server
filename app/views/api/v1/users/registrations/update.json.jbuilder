json.user do
  json.id        @user.id
  json.email     @user.email
  json.username  @user.username
  json.company   @user.company
  json.country   @user.country
  json.state     @user.state
  json.city      @user.city
  json.address1  @user.address1
  json.address2  @user.address2
  json.dial_code @user.dial_code
  json.phone     @user.phone
  json.telephone @user.telephone
  json.avatar    avatar_url_for @user
end