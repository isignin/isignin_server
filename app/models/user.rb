# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  username               :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  locked_at              :datetime
#  company                :string
#  country                :string
#  state                  :string
#  city                   :string
#  address1               :string
#  address2               :string
#  telephone              :string
#  phone                  :string
#  created_at             :datetime
#  updated_at             :datetime
#  dial_code              :string
#  api_token              :string
#  session_token          :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#

class User < ActiveRecord::Base
  # Devise
  # Include default devise modules. Others available are:
  # :omniauthable
  devise :database_authenticatable, 
         :registerable, 
         :recoverable,
         :trackable, 
         :validatable, 
         :confirmable, 
         :lockable, 
         :rememberable

  attr_readonly :email

  has_attached_file :avatar, :styles => { :medium => '250x250>', :thumb => '100x100>' }, :default_url => '/images/:style/missing.png'

  before_save do |user|
    user.email    = email.downcase
    user.username = username.downcase
  end

  has_many :attendance_relationships, foreign_key: 'attendee_id' , dependent: :destroy
  has_many :attend_events, through: :attendance_relationships, source: :event
  has_many :owned_events, foreign_key: 'owner_id', class_name: 'Event', dependent: :destroy
  has_many :microposts, dependent: :destroy

  # Validations
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  validates :username,
            uniqueness: { :case_sensitive => false },
            presence: true,
            length: { maximum: 30 },
            on: :create

  validates :email,
            uniqueness: { :case_sensitive => false },
            presence: true,
            on: :create

  validates :password,
            presence: true,
            on: :create

  validates :password_confirmation,
            presence:true,
            on: :create

  # validate telephone and phone are numbers

  # Devise overwrite
  def active_for_authentication?
    super && is_active?
  end

  def inactive_message
    if is_active?
      super
    else
      :deactivated
    end
  end

  # Helper methods
  def self.active_users
    where('locked_at IS NULL')
  end

  def self.inactive_users
    where('locked_at IS NOT NULL')
  end

  def reset_api_authentication_token!
    update_attribute(:api_token, create_token)
  end

  def reset_session_authentication_token!
    update_attribute(:session_token, create_token)
  end

  def remember_me
    true
  end

  private
  def is_active?
    if locked_at.nil?
      true
    else
      false
    end
  end

  def create_token
    SecureRandom.urlsafe_base64
  end
end
