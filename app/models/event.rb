# == Schema Information
#
# Table name: events
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  address    :string
#  start_at   :datetime         not null
#  end_at     :datetime
#  lat        :float            not null
#  lon        :float            not null
#  owner_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Event < ActiveRecord::Base
  validates :name, presence: true, length: {maximum: 100}
  validates :start_at, presence: true
  validates :lat, presence: true
  validates :lon, presence: true
  validates :owner_id, presence: true

  belongs_to :owner, class_name: 'User'
  has_many :attendance_relationships, dependent: :destroy
  has_many :attendees, through: :attendance_relationships, source: :attendee

  reverse_geocoded_by :lat, :lon
end
