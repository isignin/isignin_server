# == Schema Information
#
# Table name: attendance_relationships
#
#  id          :integer          not null, primary key
#  event_id    :integer          not null
#  attendee_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AttendanceRelationship < ActiveRecord::Base
  validates :event_id, presence: true
  validates :attendee_id, presence: true

  belongs_to :event
  belongs_to :attendee, class_name: 'User'
end
