module DeviseHelper
  def devise_error_messages!
    if resource.errors.full_messages.any?
      resource.errors.full_messages.uniq!
      flash.now[:error] = resource.errors.full_messages.join('<br/>')
    end
  end

  #make the available from non-Devise controller
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
