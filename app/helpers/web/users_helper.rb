module Web
  module UsersHelper
    # Returns the Gravatar (http://gravatar.com/) for the given user
    def gravatar_for(user, options = { size:250 })
      size = options[:size]
      if user.avatar.url.present?
        if size >= 250
          image_tag user.avatar.url(:medium)
        else
          image_tag user.avatar.url(:thumb)
        end
      else
        gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
        gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
        image_tag(gravatar_url, alt: user.username, class: 'gravatar')
      end
    end
  end
end
