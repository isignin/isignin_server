module Api
  module UsersHelper
    def avatar_url_for(user, options = { size:250 })
      if user.try(:avatar)
        size = options[:size]
        if size >= 250
          user.avatar.url(:medium)
        else
          user.avatar.url(:thumb)
        end
      else
        gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
        "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
      end
    end
  end
end